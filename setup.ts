import SerialPort from 'serialport';

import * as readline from 'readline';
import { writeFileSync, readFileSync, exists, existsSync } from 'fs';

function askQuestion(query: string): Promise<string> {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise(resolve => rl.question(query, ans => {
    rl.close();
    resolve(ans);
  }))
}

const hour = 60 * 60 * 1000;
const day = 24 * hour;
const month = 30 * day;
const year = 365 * day;

const settingsFileName = "Settings.json";
const settings = existsSync(settingsFileName) ? JSON.parse(readFileSync(settingsFileName) + "") : {};

async function main() {
  const mode = await askQuestion([
    "Which item would you like to setup?",
    "[1] Usb port",
    "[2] Current event name",
    "[3] Score History Reach",
    "[q] Quit",
    ">> "
  ].join("\n"))
  if (mode !== "q") {
    if (mode === "1") {
      const ports = await SerialPort.list();
      const portResp = await askQuestion([
        "Which port would you like to send midi to?",
        ...ports.map((port, index) => `[${index + 1}]: ${port.comName}`),
        ">> "
      ].join("\n"));
      const portNum = parseInt(portResp as any);
      if (isNaN(portNum)) { return console.error("Must be a number"); }

      const port = ports[portNum - 1];
      if (!port) { return console.error("No port of that number exists"); }

      console.log(port);
      settings.currentPort = port.comName;

    } else  if (mode === "2") {
      settings.eventName = await askQuestion([
        "What is the current event name?",
        ">> "
      ].join("\n"));

    } else  if (mode === "3") {
      const rollOffType = await askQuestion([
        "What kind of range would you like for score roll off?",
        "[h] Hours",
        "[d] Days",
        "[m] Months",
        "[y] Years",
        ">> "
      ].join("\n"));
      const num = parseFloat(await askQuestion([
        `How many ${rollOffType}?`,
        ">> "
      ].join("\n")));
      if (isNaN(num) || num <= 0) { return console.error("Value must be a postitive number"); }

      let msPerUnit;
      switch(rollOffType) {
        case "h": msPerUnit = hour; break;
        case "d": msPerUnit = day; break;
        case "m": msPerUnit = month; break;
        case "y": msPerUnit = year; break;
        default: return console.error(`Unknown value ${rollOffType}`)
      }
      settings.rollOff = Math.floor(msPerUnit * num);
    }
    writeFileSync(settingsFileName, JSON.stringify(settings));
    console.log("Settings saved", settings);
    await main();

  } else {
    console.log("Good bye!");
  }
}
main();