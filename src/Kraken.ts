import { IOInterface, Tentacle, TentacleList, Piston, Eye, EyeList, PistonList, Wave } from "./IOInterface";
export {
	Tentacle,
	TentacleList,
	Eye,
	EyeList
}

export type EyeHitListener = (eye:Eye) => void;

interface IEyeToPiston extends Record<Eye, boolean> {}

export class Kraken {
	public static eyeToPistonMap: {[key: number]: Piston | null} = {
		[Eye.BIG_MAMA] : null,
		[Eye.MAMA_LEFT] : Wave.UPPER_LEFT,
		[Eye.MAMA_RIGHT] : Wave.UPPER_RIGHT,
		[Eye.BOTTOM_LEFT] : Tentacle.BOTTOM_LEFT,
		[Eye.BOTTOM_CENTER] : Tentacle.BOTTOM_CENTER,
		[Eye.BOTTOM_RIGHT] : Tentacle.BOTTOM_RIGHT,
		[Eye.MID_LEFT] : Tentacle.MID_LEFT,
		[Eye.MID_RIGHT] : Tentacle.MID_RIGHT,
	}
	private eyeHitListeners: Array<EyeHitListener> = [];

	constructor(private io: IOInterface){
		io.onEyeHit = (eye) => {
			this.io.setEyeState(eye, false);
			this.dispatchEyeHitEvent(eye);
		}
	}

	public extendTentacle(tent: Tentacle) {
		this.io.setPistonState(tent, true);
	}

	public contractTentacle(tent: Tentacle) {
		this.io.setPistonState(tent, false);
	}

	public flipDirection(piston: Piston) {
		const state = this.io.getPistonState(piston);
		this.io.setPistonState(piston, state ? !state.on : false);
	}

	public setPistonState(piston: Piston, onOff: boolean) {
		this.io.setPistonState(piston, onOff);
	}

	public hideAndDisableAll() {
		this.allEyesRed();
		this.hideAllEyes();
	}

	public reset() {
		this.hideAllEyes();
		this.allEyesGreen();
	}

	public hideAllEyes() {
		this.io.setAllPistonStates(false);
	}

	public showAllEyes() {
		this.io.setAllPistonStates(true);
	}

	public allEyesGreen() {
		this.io.setAllEyeStates(true);
	}

	public allEyesRed() {
		this.io.setAllEyeStates(false);
	}

	public showEye(eye: Eye, shouldShow: boolean = true) {
		const piston = Kraken.eyeToPistonMap[eye];
		if (piston !== null) {
			this.setPistonState(piston, shouldShow);
		}
	}

	public justBigMamaGreen() {
		this.io.setAllEyeStates(false);
		this.io.setEyeState(Eye.BIG_MAMA, true);
	}

	public setEyeState(eye: Eye, state: "red" | "green") {
		this.io.setEyeState(eye, state === "green");
	}

	public addEyeHitListener(listener: EyeHitListener) {
		this.eyeHitListeners.push(listener);
		console.log("add listener", this.eyeHitListeners.length);
	}

	public removeEyeHitListener(listener: EyeHitListener) {
		// console.log(this.eyeHitListeners);
		console.log(listener === this.eyeHitListeners[0], listener === this.eyeHitListeners[1]);
		const target = this.eyeHitListeners.indexOf(listener);
		if (target != -1) {
			this.eyeHitListeners.splice(target, 1);
		}
	}

	public allEyesHit() { 
		return this.allTentaclesHit() && this.allMamasHit();
	}

	public allTentaclesHit() {
		return this.eyesAreHit([
			Eye.BOTTOM_LEFT, Eye.BOTTOM_CENTER, Eye.BOTTOM_RIGHT,
			Eye.MID_LEFT, Eye.MID_RIGHT 
		])
	}

	public allMamasHit() {
		return this.eyesAreHit([
			Eye.MAMA_LEFT, Eye.MAMA_RIGHT, Eye.BIG_MAMA
		]);
	}

	public bigMommaHit() {
		return this.eyesAreHit([Eye.BIG_MAMA]);
	}

	public eyesAreHit(eyes: Eye[]) {
		const notHit = eyes.filter((eye) => this.io.getEyeState(eye));
		if (notHit.length) {
			console.log("Not yet hit ", notHit);
			return false;
		} 
		return true;
	}

	private dispatchEyeHitEvent(eye: Eye) {
		console.log(this.eyeHitListeners.length);
		const staticArray = Array.from((this.eyeHitListeners));
		staticArray.forEach((listener, index) => {
			listener(eye);
		});
	}
}