import { IOInterface, Sound } from "./IOInterface";
import { wait } from "./Tools";

export class Sounds {
	constructor(private io: IOInterface) {}

	public async ringBell(durationMs: number, times: number = 1, gap: number = 250): Promise<void> {
		return this.playSound(Sound.BELL, durationMs, times, gap);
	}

	public async buzz(durationMs: number, times: number = 1, gap: number = 250): Promise<void> {
		return this.playSound(Sound.BUZZER, durationMs, times, gap);
	}

	public async playSound(sound: Sound, durationMs: number, times: number, gap: number) {
		console.log(sound === Sound.BELL ? "ring" : "buzz");
		this.io.setSoundState(sound, true);
		await wait(durationMs);
		console.log("off")
		this.io.setSoundState(sound, false);
		if (times > 1) {
			await wait(gap);
			await this.playSound(sound, durationMs, times - 1, gap);
		}
	}
}