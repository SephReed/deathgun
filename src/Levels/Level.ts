import {Deathgun} from "../Deathgun";
import { EyeHitListener, Eye } from "../Kraken";

export abstract class Level {

	// private startTime?: number;
	// private endTimer?: NodeJS.Timeout;
	private eyeHitListener: EyeHitListener;

	public onLevelEnd?: (success: boolean) => void;

	constructor (protected game: Deathgun) {
		this.eyeHitListener = this.onEyeHit.bind(this);
	}

	public run() {
		// this.startTime = Date.now();
		this.startSequence();
		// this.startEndTimer(this.lengthMs);
		this.game.kraken.addEyeHitListener(this.eyeHitListener);
	}

	public stop() {
		// this.stopEndTimer();
		this.stopSequence();
		this.game.kraken.removeEyeHitListener(this.eyeHitListener);
	}

	// public addTime(ms: number) {
	// 	if (this.startTime) {
	// 		const timeUsed = Date.now() - this.startTime;
	// 		this.stopEndTimer();
	// 		const timeLeft = (this.lengthMs + ms) - timeUsed;
	// 		this.startEndTimer(timeLeft);
	// 	}
	// 	this.lengthMs += ms;
	// }
	
	protected abstract startSequence(): void;
	protected abstract stopSequence(): void;

	protected abstract onEyeHit(eye: Eye): void;

	// private startEndTimer(ms: number) {
	// 	if (ms < 0) { return; }
	// 	this.endTimer = setTimeout(() => {
	// 		this.endTimer = undefined;
	// 		this.stopSequence();
	// 		this.levelFail();
	// 	}, ms);
	// }

	// private stopEndTimer() {
	// 	if (!this.endTimer) { return; }
	// 	clearTimeout(this.endTimer);
	// 	this.endTimer = undefined;
	// }

	// protected getPoints(): number {
	// 	return 0;
	// }

	protected levelFail() {
		this.levelEnd(false);
	}

	protected levelSuccess() {
		this.levelEnd(true);
	}

	private levelEnd(success: boolean) {
		if (this.onLevelEnd) {
			this.onLevelEnd(success);
		}
	}
}