import {Level} from "./Level";
import { AsyncMovement, PistonArgMap } from "./Phases/AsyncMovement";
import { Deathgun } from "../Deathgun";
import { EyeHitListener } from "../Kraken"
import { Eye, EyeList } from "../IOInterface"


export interface IAsyncPistonsLevelArgs {
	lengthMs?: number;
	averageIntervalMs?: number;
	state?: PistonArgMap;
	pauseBetweenHits?: boolean;
	requiredEyes?: Eye[];
	pointsPerEye: number;
}

export class AsyncPistonsLevel extends Level {
	private mainPhase: AsyncMovement;
	private paused: boolean = false;
	private hasHit = new Map<Eye, boolean>();

	constructor(game: Deathgun, private args: IAsyncPistonsLevelArgs) {
		super(game);
		this.mainPhase = new AsyncMovement(this.game, args.state);
		(args.requiredEyes || EyeList).forEach((eye) => this.hasHit.set(eye, false));
		this.hasHit.set(Eye.BIG_MAMA, true);
	}

	protected startSequence() {
		if(this.args.averageIntervalMs){
			this.mainPhase.averageInterval = this.args.averageIntervalMs;
		}
		this.game.kraken.allEyesGreen();
		this.game.kraken.setEyeState(Eye.BIG_MAMA, "red");
		this.mainPhase.start();
	}

	protected stopSequence() {
		this.mainPhase.stop();
	}

	private allHitsComplete() {
		return Array.from(this.hasHit.values()).includes(false) === false;
	}

	protected onEyeHit(eye: Eye): void {
		const { kraken } = this.game;
		const eyeAlreadyHit = this.hasHit.get(eye);
		if (eyeAlreadyHit === false) {
			this.game.addPoints(this.args.pointsPerEye);
			this.game.scoreManager.addHit();
		}
		this.hasHit.set(eye, true);
		if (this.allHitsComplete() ) {
			this.levelSuccess();

		} else if (this.args.pauseBetweenHits) {
			if (eye === Eye.BIG_MAMA) {
				if (this.paused) {
					this.hasHit.forEach((isHit, eye) => {
						kraken.setEyeState(eye, isHit ? "red" : "green");
					})
					this.mainPhase.start();
				}
				this.paused = false;

			} else if (eyeAlreadyHit === false) {
				this.mainPhase.pause();
				kraken.hideAllEyes();
				kraken.justBigMamaGreen();
				this.paused = true;
			}
		}
	}
}