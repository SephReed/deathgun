import { Level } from "./Level";
import { Deathgun } from "../Deathgun";
import { cancelable, Waiter } from "../Tools";
import { Eye, EyeList } from "../Kraken";


export interface IRandomGalleryArgs {
  startingExtendMs: number;
  extendMsDrop: number;

  startingSpacingdMs: number;
  spacingMsDrop: number;
  
  startingHitPoints: number;
  hitPointsIncrease: number;
}

type EyeState = "hidden" | "reveal" | "hit" | "out";

export class RandomGallery extends Level {
  private waiter?: Waiter;
  private eyeStates = new Map<Eye, EyeState>();
  private hitPoints = 10;

  constructor (game: Deathgun, private args: IRandomGalleryArgs) {
    super(game);
  }

  protected startSequence(): void {
    let extendMs = this.args.startingExtendMs;
    let spacingMs = this.args.startingSpacingdMs;
    this.hitPoints = this.args.startingHitPoints;

    EyeList.forEach((eye) => this.eyeStates.set(eye, "hidden"));
    
    cancelable(async (waiter) => {
			const { kraken } = this.game;
			if (this.waiter) { this.waiter.stop(); }

			this.waiter = waiter;
			kraken.allEyesRed();
      kraken.hideAllEyes();

      

      while (true) {
        let hiddenEyes: Eye[] = [];
        let secondLoop = false;
        while (hiddenEyes.length === 0) {
          if (secondLoop) {
            await waiter.wait(250);
          }
          const inGameEyes = Array.from(this.eyeStates.keys())
            .filter((eye) => this.eyeStates.get(eye) !== "out");
  
          if (inGameEyes.length <= 2) {
            console.log("Too few eyes", inGameEyes);
            this.levelSuccess();
            this.stopSequence();
            return;
          }

          hiddenEyes = Array.from(this.eyeStates.keys())
            .filter((eye) => this.eyeStates.get(eye) === "hidden");
          
          secondLoop = true;
        }

        const target = Math.floor(Math.random() * hiddenEyes.length);
        const eye = hiddenEyes.splice(target, 1)[0];
        // this.eyesHit.set(eye, false)
        this.eyeStates.set(eye, "reveal");

        kraken.showEye(eye);
        kraken.setEyeState(eye, "green");

        waiter.wait(extendMs).then(async () => {
          kraken.showEye(eye, false);
          await waiter.wait(1200);
          kraken.setEyeState(eye, "red");
          if (this.eyeStates.get(eye) !== "hit") {
            this.eyeStates.set(eye, "out");
            kraken.showEye(eye);
            // this.deadEyes.push(eye);
          } else {
            this.eyeStates.set(eye, "hidden");
            // this.inGameEyes.push(eye);
          }
        });
        await waiter.wait(spacingMs);

        extendMs -= this.args.extendMsDrop;
        spacingMs -= this.args.spacingMsDrop;
      }
    });
  }  
  
  protected stopSequence(): void {
    if (this.waiter) { this.waiter.stop(); }
  }

  protected onEyeHit(eye: Eye): void {
    if (this.eyeStates.get(eye) === "reveal") {
      this.game.scoreManager.addHit();
      this.game.addPoints(this.hitPoints);
      this.hitPoints *= this.args.hitPointsIncrease;
      this.eyeStates.set(eye, "hit");
      this.game.kraken.showEye(eye, false);
      this.game.kraken.setEyeState(eye, "red");
    }
  }

}