import { Level } from "./Level";
import { Deathgun } from "../Deathgun";
import { ISpeedComboArgs, SpeedCombo } from "./Phases/SpeedCombo";
import { CancelToken, allowCancel, wait, Waiter } from "../Tools";
import { Phase } from "./Phases/Phase";
import { EyeHitListener, Eye } from "../Kraken";


export interface ISpeedCombosLevelArgs {
  combos: ISpeedComboArgs[];
}

export class SpeedCombosLevel extends Level {
  private currentPhase?: SpeedCombo;
  private waiter?: Waiter;

  constructor (game: Deathgun, private args: ISpeedCombosLevelArgs) {
    super(game);
  }

  protected async startSequence(): Promise<void> {
    if (this.waiter) { this.waiter.stop(); }
    const waiter = this.waiter = new Waiter();

    const { combos } = this.args;
    for (let i = 0; i < combos.length; i++) {
      const combo = combos[i];
      this.currentPhase = new SpeedCombo(this.game, combo);
      this.currentPhase.start(waiter);
      await this.currentPhase.end();
      this.game.addPoints(combo.pointsPerEye * combo.eyes.length);
      this.currentPhase = undefined;
      await waiter.wait(1000);
    }

    this.levelSuccess();
  }  
  
  protected stopSequence(): void {
    if (this.waiter) { this.waiter.stop();  }
    if (this.currentPhase) { this.currentPhase.stop(); }
  }

  protected onEyeHit(eye: Eye): void {
    if (this.currentPhase) {
      this.currentPhase.hitEye(eye);
    }
  }

}