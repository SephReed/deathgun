import {Phase} from "./Phase";
import { Deathgun } from "../../Deathgun";
import { Eye } from "../../Kraken";
import { cancelable, Waiter } from "../../Tools";


export interface ISpeedComboArgs {
	eyes: Eye[];
	msExtend: number;
	msBetween: number;
	pointsPerEye: number;
	randomizeOrder?: boolean;
}

export class SpeedCombo extends Phase {
	private leadWaiter?: Waiter;
	private waiter?: Waiter;
	private eyesHit = new Map<Eye, boolean>();

	constructor(game: Deathgun, private args: ISpeedComboArgs) {
		super(game);
	}

	public async start(boss?: Waiter) {
		this.ending = undefined;
		
		const { kraken } = this.game;
		if (this.waiter) { this.waiter.stop(); }
		if (!this.leadWaiter && boss) { this.leadWaiter = boss.sub() }

		const waiter = this.waiter = (this.leadWaiter as any).sub();
		kraken.allEyesRed();
		kraken.hideAllEyes();
		await waiter.wait(1000);
		this.eyesHit = new Map();

		let eyes = Array.from(this.args.eyes);
		if (this.args.randomizeOrder) {
			let pullFrom = eyes;
			eyes = [];
			while(pullFrom.length) {
				const target = ~~(Math.random() * pullFrom.length);
				eyes = eyes.concat(pullFrom.splice(target, 1));
			}
		}

		eyes.forEach((eye) => this.eyesHit.set(eye, false));

		const popInOut = [];
		for (let i = 0; i < eyes.length; i++) {
			const eye = eyes[i];
			kraken.setEyeState(eye, "green");
			kraken.showEye(eye);
			popInOut.push(
				waiter.wait(this.args.msExtend)
				.then(async () => {
					console.log(waiter);
					kraken.showEye(eye, false);
					await waiter.wait(1000);
					if (this.eyesHit.get(eye) === false) {
						this.game.sounds.buzz(100);
						this.start();
					}
				})
			);
			await waiter.wait(this.args.msBetween);
		}

		await Promise.all(popInOut);
		await waiter.wait(1000);
		if (this.ending) { return; }
		this.game.sounds.buzz(100);
		this.start();
	}

	public pause() {
		this.stop();
	}

	public stop() {
		console.log("HAS BEEN STOPPED");
		// this.ending = undefined;
		if (this.leadWaiter) {
			this.leadWaiter.stop();
			this.leadWaiter = undefined;
		}
	}

	public hitEye(eye: Eye) {
		console.log("HIT EYE");
		if (this.eyesHit.has(eye)) {
			if (this.eyesHit.get(eye) === false) {
				this.game.scoreManager.addHit();
			}
			this.eyesHit.set(eye, true);
			const allEyesHit = Array.from(this.eyesHit.values()).includes(false) === false;
			console.log("eyes hit", allEyesHit);
			if (allEyesHit) {
				this.game.sounds.ringBell(100);
				this.setEnding("Success");
			}
		}
	}

}