import { Phase } from "./Phase";
import { PistonList, Piston, Tentacle, Eye } from "../../IOInterface";
import { Deathgun } from "../../Deathgun";
import { EyeHitListener } from "../../Kraken";


export interface IPistonArgs {
	intervalMs: number;
	pw: number;
	count: number;
}

export interface IAsyncPistonState {
	growMs: number;
	shrinkMs: number;
	lastFlip: number;
	timeout?: NodeJS.Timeout;
	count: number;
	on: boolean;
}



export type PistonArgMap = Map<Piston, IPistonArgs>;

export class AsyncMovement extends Phase {
	private pistonIntervals: Map<Piston, IAsyncPistonState> = new Map();
	public averageInterval = 1000;
	public intervalSpread = 200;


	constructor(game: Deathgun, private pistonArgs?: PistonArgMap) {
		super(game);


		console.log(pistonArgs);

		PistonList.forEach((piston) => {
			if (pistonArgs && pistonArgs.has(piston)) {
				const args = pistonArgs.get(piston);
				if (args) {
					const growMs = args.pw * args.intervalMs;
					const shrinkMs = args.intervalMs - growMs;
					args.count %= args.intervalMs;
					this.pistonIntervals.set(piston, {
						growMs,
						shrinkMs,
						count: args.count % growMs,
						on: args.count > growMs,
						lastFlip: Date.now() - args.count,
						timeout: undefined,
					})
				}
			} else {
				console.log(piston);
				throw new Error("Reguires all pistons to be set");
				// const spread = this.intervalSpread;
				// const interval = this.averageInterval - (spread / 2) + (spread * Math.random());
				// this.pistonIntervals.set(piston, {
				// 	on: true,
				// 	growMs: interval * 0.5,
				// 	shrinkMs: interval * 0.5,
				// 	lastFlip: 0,
				// 	timeout: undefined,
				// 	count: 0,
				// })
			}
		});
	}

	public start() {
		this.pistonIntervals.forEach((state, piston) => {
			const { kraken } = this.game;
			const startNextTimeout = () => {
				kraken.setPistonState(piston, state.on);
				const fullTime = state.on ? state.growMs : state.shrinkMs;
				state.timeout = setTimeout(() => {
					state.on = !state.on;
					state.count = 0;
					state.lastFlip = Date.now();
					startNextTimeout();
				}, fullTime - state.count);
			}

			startNextTimeout();
		});		
	}

	public pause() {
		const now = Date.now();
		this.pistonIntervals.forEach((state, piston) => {
			state.count = now - state.lastFlip;
			if (state.timeout) {			
				clearTimeout(state.timeout);
				state.timeout = undefined;
			}
		});
	}

	public stop() {
		this.pause();
		this.pistonIntervals.forEach((state, piston) => {
			state.count = 0;
			state.on = true;
		});
	}
}