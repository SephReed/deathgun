import { Deathgun } from "../../Deathgun";

export type Ending = "Success" | "Fail";
type EndingListener = (end: Ending) => any;

export abstract class Phase {
	protected ending?: Ending;
	private endListeners: EndingListener[] = [];

	constructor(protected game: Deathgun) {}
	public abstract start(): void;
	public abstract pause(): void;
	public abstract stop(): void;

	public onEnd(cb: EndingListener) {
		if (this.ending) {
			cb(this.ending);
		} else {
			this.endListeners.push(cb);
		}
	}

	public end(): Promise<Ending> {
		return new Promise((res) => {
			this.onEnd(res);
		});
	}

	protected setEnding(ending: Ending) {
		this.ending = ending;
		this.endListeners.forEach((cb) => cb(ending));
	}
}