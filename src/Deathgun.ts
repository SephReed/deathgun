const asciiArt = () => console.log(logoAscii );


const everyNLine = (str: string, n: number) => str.split(/\n/g).filter((_, index) => (index % n) === 0).join("\n");


const logoAscii = String.raw`
                           @@@@                       @@@@@@			    
    @@@             _____    @@@@@@@@@@        	        @@@@@@@@@@@@		   
     @@@@@@@       /.....\   _______________@@____________@@@@___@@@@___@@@___    ________   ___
      @@@@@@@@@@@@/..__...|@/......./......|@'........|...|@@|...|@-'...'-|...|@@|...|....\ |...|
       @@@@@@@@@@/..|@@|..|/...____/.._....|/__ ...___|...|__|...|/...____|...|@@|...|.....\|...|
        @@@@@@@@/...|@@|..|.../###/../@|...|@@/...'@@@|..........|.../____|...\__/...|..........|
         @@@@@@/.../@@/../......./../##|...|@@'.../@ |....__....|...||__..|\......../|...|\.....|@
          @@@#/.../@@/../...____/..........|@/...'@@@|...|@@|...|\......../ '-_____/@|___|@\____|@@@@
            @/.../##/../.../###/....___ ...|@'.../@  |___|@@|___|@'-____-'               @@@@@@@@@@@@@@@@
            /........-/......./..../@@@|___|/___'@@      @@@@@@@@@@@@@@
           /_______-'/_______/____/@@@@@@@@@@@@@@@            @@@@@@@@@@
                                        @@@@@@@@@@@                @@@@@@
                                            @@@@@@@@
`.replace(/\./g, " ");


import { Kraken } from "./Kraken";
import { Sounds } from "./Sounds";
import { IOInterface, Tentacle, TentacleList, WaveList, Wave, Eye } from "./IOInterface";
import * as Levels from "./Levels";
import { ILevelConfig,
	IntroLevel, BeginnerMid, BeginnerFinal,
	IntermediateMid,
	IntermediateFinal,
	ExpertFinal, 
	} from "./LevelConfigs";
import { CancelToken, wait, allowCancel, stopIfCancelled, cancelFn } from "./Tools";
import { ScoresManager } from "./ScoresManager";




const gamePath:ILevelConfig = {
	...IntroLevel,
	branches: [
		{
			...BeginnerMid,
			branches: [
				{
					...BeginnerFinal
				},
				{
					scoreNeeded: 80000,
					...IntermediateFinal
				},
			]
		},
		{
			scoreNeeded: 45000,
			...IntermediateMid,
			branches: [
				{
					...BeginnerFinal
				},
				{
					scoreNeeded: 135000,
					...IntermediateFinal
				},
				{
					scoreNeeded: 150000,
					...ExpertFinal
				}
			]
		}
	]
}

console.log(process.argv[2]);

// const gamePath = ExpertFinal;
type GameState = "WAITING" | "PLAYING" | "WIN" | "LOSE";

export class Deathgun {
	private io = new IOInterface();
	public readonly kraken = new Kraken(this.io);
	public readonly sounds = new Sounds(this.io);
	public readonly scoreManager = new ScoresManager(this.io);
	private currentLevelConfig?: ILevelConfig;
	private currentLevel?: Levels.Level;
	private gameCancelToken?: CancelToken;
	// private nextLevelConfig?: ILevelConfig;
	private gameState: GameState = "WAITING";
	// private currentPoints: number = 0;

	private gameLengthMs = 60 * 1000;
	private gameStartTime: number = 0;

	constructor(gameType: string = "NORMAL") {
		if (gameType === "NORMAL") {
			this.io.onResetRequest = () => {
				this.resetGame();
			}
		}
	}

	public start() {
		console.log("Get ready for...")
		asciiArt();

		this.resetGame();
	}


	public resetGame(doCoolDown?: boolean) {
		console.log("Stopping everything");
		this.kraken.hideAndDisableAll();
		this.io.setStageFx(false);

		if (this.gameCancelToken) { this.gameCancelToken.cancel(); }
		if (this.currentLevel) { this.currentLevel.stop(); }
		this.currentLevelConfig = undefined;

		allowCancel(async (cancelToken) => {
			console.log("Game reset");
			this.gameCancelToken = cancelToken;
	
			if (doCoolDown) { await wait(15000, cancelToken); }
			
			// MAIN GAME LOOP
			let levelNum = 0;
			const startNextLevel = async () => {
				this.gameState = "PLAYING";
				this.io.setStageFx(true);
				this.kraken.reset();
				if (this.currentLevel) { this.currentLevel.stop(); }
	
				let nextLevel: ILevelConfig;
				if (this.currentLevelConfig) {
					if (this.currentLevelConfig.branches) {
						const availableBranches = this.currentLevelConfig.branches.filter(
							(config) => (config.scoreNeeded || 0) <= this.scoreManager.getCurvedScore(),
						).sort((a, b) => (b.scoreNeeded || 0) - (a.scoreNeeded || 0));
						console.log(availableBranches);
						nextLevel = availableBranches[0];
					} else {
						return this.victory(cancelToken);
					}
				} else {
					nextLevel = gamePath;
				}
				
				levelNum++;
				this.sounds.ringBell(1000, levelNum, 1000);
				if (levelNum > 1) {
					await this.waitForLevelStart();
					await stopIfCancelled(cancelToken);
				}
				
				console.log(`Starting level ${nextLevel.name}`);
				this.currentLevelConfig = nextLevel;
				this.currentLevel = this.currentLevelConfig.createLevel(this);
				this.currentLevel.onLevelEnd = cancelFn(async (success) => {
						await stopIfCancelled(cancelToken);
						console.log("Level End");
						if (success) {
							this.addTimePoints(nextLevel.timeBonusMultiplier);
							startNextLevel() 
						} else {
							this.gameOver(cancelToken);
						}
					});
				
				this.currentLevel.run();
			}
	
			this.gameState = "WAITING";
			await this.waitForLevelStart();
			this.gameStartTime = Date.now();
			await stopIfCancelled(cancelToken);
			this.startTimer(cancelToken);
			this.scoreManager.resetPoints();
			startNextLevel();
		})
	}

	public addPoints(points: number) {
		this.scoreManager.addPoints(points);
	}

	private addTimePoints(multiplier: number) {
		const timeLeft = (this.gameStartTime + (60 * 1000)) - Date.now();
		console.log("Time left after level ", timeLeft);
		this.addPoints(timeLeft * multiplier / 10);
	}

	// private async pushPoints() {
	// 	const curvedScore = Math.pow(this.currentPoints, 1.23456789);
	// 	this.scoreManager.addScore(Math.floor(curvedScore));
	// 	this.currentPoints = 0;
	// }

	private async startTimer(cancelToken: CancelToken) {
		await wait(this.gameLengthMs, cancelToken);
		if (this.gameState === "PLAYING") {
			return this.gameOver(cancelToken);
		}
	}
	
	private async gameOver(cancelToken: CancelToken) {
		if (this.currentLevelConfig && this.currentLevelConfig.selfTerminating) { return; }
		if (this.currentLevel) { this.currentLevel.stop(); }
		this.gameState = "LOSE";
		console.log("GAME OVER :(");
		const scoreType = this.scoreManager.pushScore(true);
		this.sounds.buzz(1500);
		await this.doLoseAnimation("normal", cancelToken);
		if (scoreType !== "UNREMARKABLE") { await wait(5000, cancelToken); }
		return this.resetGame(true);
	}

	private async victory(cancelToken: CancelToken) {
		console.log("YOU WIN!!!");
		this.gameState = "WIN";
		this.sounds.ringBell(1000, 5, 1000);
		const scoreType = this.scoreManager.pushScore(false);
		await this.doWinAnimation("normal", cancelToken);
		if (scoreType !== "UNREMARKABLE") { await wait(10 * 1000, cancelToken); }
		return this.resetGame(true);
	}

	private waitForLevelStart() {
		this.kraken.justBigMamaGreen();
		return new Promise((resolve) => {
			const listener = (eye: Eye) => {
				if (eye === Eye.BIG_MAMA) {					
					this.kraken.removeEyeHitListener(listener);
					resolve();
				}
			}
			this.kraken.addEyeHitListener(listener);
		})
	}

	private async doWinAnimation(type: "normal", cancel: CancelToken) {
		const {kraken} = this;
		if (type === "normal") {
			for (let i = 0; i < 6; i++) {
				kraken.allEyesGreen();
				kraken.showAllEyes();
				await wait(1000, cancel);
				kraken.allEyesRed();
				kraken.hideAllEyes();
				await wait(1000, cancel);
			}
		}
	}

	private async doLoseAnimation(type: "normal", cancel: CancelToken) {
		const {kraken} = this;
		if (type === "normal") {
			kraken.allEyesRed();
			kraken.showAllEyes();
			await wait(5000, cancel);
			kraken.hideAllEyes();
		}
	}
}