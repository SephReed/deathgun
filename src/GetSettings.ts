import { existsSync, readFileSync } from "fs";
import { join } from "path";

const SettingsPath = join(__dirname, "../Settings.json");
if (!existsSync(SettingsPath)) {
  throw new Error(`Please run: "npm run setup" before running game`)
}

const Settings: {
  currentPort: string;
  rollOff: number;
  eventName: string;
} = JSON.parse(readFileSync(SettingsPath).toString());

export { Settings };

const issues = [];
if (!Settings.currentPort) { issues.push("Current port not set"); }
if (!Settings.rollOff) { issues.push("Highscore roll off not set"); }
if (!Settings.eventName) { issues.push("Event name not set"); }

if (issues.length) {
  issues.forEach((issue) => console.error(issue));
  throw new Error(`Please run: "npm run setup" before running game`);
}