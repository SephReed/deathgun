import * as WebSocket from "ws";
import SerialPort from 'serialport';
const Settings = require("../Settings.json");

SerialPort.list().then((list) => console.log(list));
const myPort = new SerialPort(Settings.currentPort, { baudRate: 57600 }, (err) => {
	if (err) {
		console.log("NO PORT", err);
	} else {
		console.log("GOT PORT");
	}
});



export enum Tentacle {
	BOTTOM_LEFT = 0,
	BOTTOM_CENTER = 1,
	BOTTOM_RIGHT = 2,

	MID_RIGHT = 4,
	MID_LEFT = 3,
}
export const TentacleList = [
	Tentacle.MID_LEFT, Tentacle.MID_RIGHT,
	Tentacle.BOTTOM_LEFT, Tentacle.BOTTOM_CENTER, Tentacle.BOTTOM_RIGHT
];


export enum Wave {
	UPPER_LEFT = 5,
	UPPER_RIGHT = 6
}
export const WaveList = [Wave.UPPER_LEFT, Wave.UPPER_RIGHT];


export type Piston = Tentacle | Wave;
export const PistonList = ([] as Piston[]).concat(WaveList).concat(TentacleList);

export enum Eye {
	BOTTOM_LEFT = Tentacle.BOTTOM_LEFT,
	BOTTOM_CENTER = Tentacle.BOTTOM_CENTER,
	BOTTOM_RIGHT = Tentacle.BOTTOM_RIGHT,

	MID_LEFT = Tentacle.MID_LEFT,
	MID_RIGHT = Tentacle.MID_RIGHT,

	MAMA_LEFT = Wave.UPPER_LEFT,
	MAMA_RIGHT = Wave.UPPER_RIGHT,

	BIG_MAMA = 7,
}
export const EyeList = [
	Eye.BOTTOM_LEFT, Eye.BOTTOM_CENTER, Eye.BOTTOM_RIGHT,
	Eye.MID_LEFT, Eye.MID_RIGHT,
	Eye.MAMA_LEFT, Eye.MAMA_RIGHT, Eye.BIG_MAMA,
];

function getEyeName(eye: Eye): string {
	switch(eye) {
		case Eye.MAMA_LEFT: return "Mama Left";
		case Eye.MAMA_RIGHT: return "Mama Right";
		case Eye.BIG_MAMA: return "Big Mama";

		case Eye.MID_LEFT: return "Mid Left";
		case Eye.MID_RIGHT: return "Mid Right";

		case Eye.BOTTOM_LEFT: return "Bottom Left";
		case Eye.BOTTOM_CENTER: return "Bottom Center";
		case Eye.BOTTOM_RIGHT: return "Bottom Right";
	}
	return "Uknown Eye";
}



const MUTE_SOUNDS = false;
export enum Sound {
	BELL = 1,
	BUZZER = 0
}
export const SoundList = [ Sound.BELL, Sound.BUZZER ];


export enum StageItem {
	// LIGHT = 0,
	WAVE_TOP = 4,
	WAVE_MID = 5,
	WAVE_BOTTOM = 6,
	MOMMA = 7,
}
export const StageItemList = [ 
	// StageItem.LIGHT,
	StageItem.WAVE_BOTTOM, StageItem.WAVE_MID, StageItem.WAVE_TOP,
	StageItem.MOMMA,
];


const NOTE_ON_CH_0 = 0b1001 << 4;
const NOTE_OFF_CH_0 = 0b1000 << 4;
const PISTON_CH = 0;
const EYE_CH = 1;
const STAGE_CH = 2;
const GUN_CH = 3;

interface IPistonState {
	on: boolean,
	pos: number,
	lastUpdate: number,
}


export class IOInterface {
	private webSocketServer: WebSocket.Server;
	private sockets: WebSocket[] = [];
	private extensionTimeMs = 1000;
	private contractionTimeMs = 1000;
	private pistonStates: Map<Piston, IPistonState> = new Map();
	private eyeStates: Map<Eye, boolean> = new Map();
	private soundStates: Map<Sound, boolean> = new Map();
	public onEyeHit?: (eye: Eye) => void;
	public onShot?: () => void;
	public onResetRequest?: () => void;

	constructor() {
		this.webSocketServer = new WebSocket.Server({ port: 8080 });

		this.webSocketServer.on('connection', (ws: WebSocket) => {
			this.sockets.push(ws);
			if (this.onResetRequest) {
				this.onResetRequest();
			}
		  ws.on('message', (msg) => {
		  	const [data1, data2, data3] = String(msg).split(/ +/g).map((str) => parseInt(str));
		  	this.midiIn(data1, data2, data3);
		  });
		  ws.on("close", () => this.sockets = this.sockets.filter((it) => it != ws));
		});

		let count = 0;
		let interval = setInterval(() => {
			count++;
			if (count >= 20) {
				console.error("Can not find open port");
				clearInterval(interval);
				return;
			}
			if (myPort.isOpen) {
				clearInterval(interval);
				console.log("Got open port");
				if (this.onResetRequest) {
					this.onResetRequest();
				}
				const buffer: number[] = [];
				const tryDumpBuffer = () => {
					while (buffer.length >= 3) {
						const msg = buffer.splice(0, 3);
						this.midiIn(msg[0], msg[1], msg[2]);
					}
				}
				myPort.on('data', (data) => {
					const addMe = new Uint8Array(data);
					addMe.forEach((num) => buffer.push(num));
					console.log(addMe);
					tryDumpBuffer();
				});
			} else {
				console.log("Port not yet open");
			}
		}, 500);
		

		PistonList.forEach((piston) => 
			this.pistonStates.set(piston, {
				on: false,
				pos: 0,
				lastUpdate: 0,
			}));

		this.setAllEyeStates(false);
		this.setAllSoundStates(false);
		
	}

	// Pistons
	public getPistonState(piston: Piston): IPistonState {
		const state = this.pistonStates.get(piston) as IPistonState;
		// this.updateState(state);
		return state;
	} 

	private updateState(state: IPistonState) {
		const dms = Date.now() - state.lastUpdate;
		const dpos = dms / (state.on ? this.extensionTimeMs : -this.contractionTimeMs);
		state.pos = Math.max(0, Math.min(state.pos + dpos, 1))
		state.lastUpdate = Date.now();
	}

	public setPistonState(piston: Piston, onOff: boolean) {
		this.getPistonState(piston).on = onOff;
		this.defaultMidiOut(onOff, PISTON_CH, piston);
	}

	public setAllPistonStates(onOff: boolean) {
		PistonList.forEach((piston) => this.setPistonState(piston, onOff));
	}

	// Eyes
	public getEyeState(eye: Eye): boolean {
		return this.eyeStates.get(eye) as boolean;
	} 

	public setEyeState(eye: Eye, onOff: boolean) {
		this.eyeStates.set(eye, onOff);
		this.defaultMidiOut(!onOff, EYE_CH, eye);
	}

	public setAllEyeStates(onOff: boolean) {
		EyeList.forEach((eye) => this.setEyeState(eye, onOff));
	}

	// Sounds
	public getSoundState(sound: Sound): boolean {
		return this.soundStates.get(sound) as boolean;
	} 

	public setSoundState(sound: Sound, onOff: boolean) {
		if (MUTE_SOUNDS) { return; }
		this.soundStates.set(sound, onOff);
		this.defaultMidiOut(onOff, STAGE_CH, sound);
	}

	public setAllSoundStates(onOff: boolean) {
		SoundList.forEach((sound) => this.soundStates.set(sound, onOff));
	}

	// Gun
	public setGun(onOff: boolean) {
		this.defaultMidiOut(onOff, GUN_CH, 0);
	}

	// Stage
	public setStageFx(onOff: boolean) {
		this.setAllSoundStates(onOff);
		StageItemList.forEach((item) => this.defaultMidiOut(onOff, STAGE_CH, item));
		// this.setGun(onOff);
	}
	
	// MIDI
	private defaultMidiOut(onOff: boolean, channel: number, note: number) {
		this.midiOut((onOff ? NOTE_ON_CH_0 : NOTE_OFF_CH_0) + channel, 
			note, 
			0,
		);
	}

	private midiOut(data1: number, data2: number, data3: number) {
		// TODO
		// console.log("OUT", data1, data2, data3);
		const out = [data1, data2, data3];
		this.sockets.forEach((socket) => socket.send(out.join(" ")));
		if (myPort.isOpen) { myPort.write(Buffer.from(out)); }
	}

	private midiIn(data1: number, data2: number, data3: number) {
		console.log("IN", data1, data2, data3);
		const status = (data1 & 0b11110000);
		const channel = (data1 & 0b00001111);
		console.log(`Status: ${status} -- Channel: ${channel}`);

		if (status !== NOTE_ON_CH_0) {
			console.error("Unexpected Status", status, channel);
		} else if (channel === EYE_CH && EyeList.includes(data2) && this.onEyeHit) {
			console.log(`Eye hit ${getEyeName(data2)}`);
			this.onEyeHit(data2);
		} else {
			console.log("Non eye hit message", data1, data2, data3);
			if (this.onShot) { this.onShot(); }
		}
	}
}