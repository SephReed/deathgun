import {Deathgun} from "./Deathgun";
import * as Levels from "./Levels";
import { IOInterface, Tentacle, TentacleList, WaveList, Wave, Eye, EyeList } from "./IOInterface";


export interface ILevelConfig {
	name: string;
	createLevel: (game: Deathgun) => Levels.Level;
	scoreNeeded?: number;
	branches?: ILevelConfig[];
	timeBonusMultiplier: number;
	selfTerminating?: true;
}

export const IntroLevel :ILevelConfig = {
	name: "Intro",
	createLevel: (game) => new Levels.AsyncPistonsLevel(game, { 
		state: new Map([
			...TentacleList.map((tentacle, index) => {
				const intervalMs = 3000 + (index * 200);
				const count = index * 400;
				const pw = 1500 / intervalMs;
				return [tentacle, {intervalMs, count, pw}] as any;
			}),
			...WaveList.map((wave) => [ wave, 
				wave === Wave.UPPER_LEFT ? (
					{count: 100, intervalMs: 2200, pw: 500/2200}
				):(
					{count: 0, intervalMs: 2000, pw: 500/2000}
				)
			])
		]),
		requiredEyes: EyeList.filter((it) => it !== Eye.MAMA_LEFT && it !== Eye.MAMA_RIGHT),
		pointsPerEye: 100,
	}),
	timeBonusMultiplier: 1,
}

export const BeginnerMid :ILevelConfig = {
	name: "Beginner Mid",
	createLevel: (game) => new Levels.AsyncPistonsLevel(game, { 
		state: new Map([
			...TentacleList.map((tentacle, index) => {
				const intervalMs = 3000 + (index * 200);
				const count = index * 400;
				const pw = 1500 / intervalMs;
				return [tentacle, {intervalMs, count, pw}] as any;
			}),
			...WaveList.map((wave) => [ wave, 
				wave === Wave.UPPER_LEFT ? (
					{count: 100, intervalMs: 3000, pw: 1500/3000}
				):(
					{count: 0, intervalMs: 4000, pw: 1500/4000}
				)
			])
		]),
		pauseBetweenHits: true,
		pointsPerEye: 125,
	}),
	timeBonusMultiplier: 1.1,
}

export const BeginnerFinal :ILevelConfig = {
	name: "Beginner Final",
	createLevel: (game) => new Levels.AsyncPistonsLevel(game, { 
		state: new Map([
			...TentacleList.map((tentacle, index) => {
				const intervalMs = 3000 - (index * 200);
				const count = index * 200;
				const pw = 800 / intervalMs;
				return [tentacle, {intervalMs, count, pw}] as any;
			}),
			...WaveList.map((wave) => [wave, 
				wave === Wave.UPPER_LEFT ? (
					{count: 100, intervalMs: 3000, pw: 1000/3000}
				):(
					{count: 0, intervalMs: 4000, pw: 1000/4000}
				)
			])
		]),
		pauseBetweenHits: true,
		pointsPerEye: 150,
	}),
	timeBonusMultiplier: 1.2,
}




const {
	MAMA_LEFT, BIG_MAMA, MAMA_RIGHT,
	MID_LEFT, MID_RIGHT,
	BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT
} = Eye;

export const IntermediateMid :ILevelConfig = {
	name: "Intermediate Mid",
	createLevel: (game) => new Levels.SpeedCombosLevel(game, { 
		combos: [
			{ eyes: [MID_LEFT, BOTTOM_CENTER, MID_RIGHT], 
				randomizeOrder: true,
				msExtend: 2000, msBetween: 300, pointsPerEye: 200 },

			{ eyes: [BOTTOM_LEFT, MAMA_LEFT, MAMA_RIGHT, BOTTOM_RIGHT], 
				msExtend: 2500, msBetween: 250, pointsPerEye: 240},

			{ eyes: [BOTTOM_LEFT, MID_RIGHT, BOTTOM_CENTER, MAMA_LEFT, BOTTOM_RIGHT],
				randomizeOrder: true, 
				msExtend: 2200, msBetween: 800, pointsPerEye: 300},

			{ eyes: [MAMA_RIGHT, MID_RIGHT, BOTTOM_RIGHT, BOTTOM_CENTER, BOTTOM_LEFT, MID_LEFT, MAMA_LEFT, BIG_MAMA], 
				msExtend: 2200, msBetween: 700, pointsPerEye: 350}
		]
	}),
	timeBonusMultiplier: 1.4,
}



export const IntermediateFinal :ILevelConfig = {
	name: "Intermediate Final",
	createLevel: (game) => new Levels.SpeedCombosLevel(game, { 
		combos: [
			{ eyes: [MID_LEFT, BOTTOM_CENTER, MID_RIGHT, MAMA_LEFT], 
				randomizeOrder: true,
				msExtend: 2000, msBetween: 250, pointsPerEye: 240 },

			{ eyes: [BOTTOM_LEFT, MAMA_LEFT, MAMA_RIGHT, BOTTOM_RIGHT, BOTTOM_CENTER], 
				msExtend: 2000, msBetween: 300, pointsPerEye: 300},

			{ eyes: [BOTTOM_LEFT, MID_RIGHT, BOTTOM_CENTER, MAMA_LEFT, BOTTOM_RIGHT],
				randomizeOrder: true, 
				msExtend: 1500, msBetween: 650, pointsPerEye: 350},

			{ eyes: [MAMA_RIGHT, MID_RIGHT, BOTTOM_RIGHT, BOTTOM_CENTER, BOTTOM_LEFT, MID_LEFT, MAMA_LEFT], 
				randomizeOrder: true,
				msExtend: 2200, msBetween: 600, pointsPerEye: 450}
		]
	}),
	timeBonusMultiplier: 1.7,
}




export const ExpertFinal :ILevelConfig = {
	name: "Intermediate Final",
	createLevel: (game) => new Levels.RandomGallery(game, { 
		startingExtendMs: 2200,
		extendMsDrop: 25,
		startingSpacingdMs: 1000,
		spacingMsDrop: 20,
		startingHitPoints: 150,
		hitPointsIncrease: 1.05,
	}),
	timeBonusMultiplier: 0,
	selfTerminating: true,
}