import * as fs from "fs";
import { join as pathJoin } from "path";
import { Settings } from "./GetSettings";
import * as WebSocket from "ws";
import { IOInterface } from "./IOInterface";

const historyDir = pathJoin(__dirname, "../history");

const hour = (60 * 60 * 1000);

type ScoreType = "HIGH_SCORE" | "AVG_SCORE" | "UNREMARKABLE";

type IScoreUpdate = {
  type: "current_points";
  data: number;
} | {
  type: "high_scores" | "average_scores",
  data: IScore[]
} | {
  type: "new_score",
  data: IScore & { rank: number, type?: ScoreType},
} | {
  type: "stats",
  data: IGameStats
}

interface IScore {
  date: Date;
  player?: string;
  points: number;
  shots: number;
  hits: number;
  loser: boolean;
}

interface IGameStats {
  plays: number,
  shots: number,
  hits: number,
  wins: number,
};

export class ScoresManager {
  private webSocketServer: WebSocket.Server;
	private sockets: WebSocket[] = [];

  private currentFileName: string;
  private currentFileScores: IScore[] = [];
  private allScores: IScore[] = [];

  private currentPoints: number = 0;
  private currentShots: number = 0;
  private currentHits: number = 0;

  private gameStats: IGameStats = {
    plays: 0,
    shots: 0,
    hits: 0,
    wins: 0,
  };

  constructor(io: IOInterface) {
    io.onShot = () => this.currentShots++;

    this.webSocketServer = new WebSocket.Server({ port: 8081 });

		this.webSocketServer.on('connection', (ws: WebSocket) => {
      this.sockets.push(ws);
      this.sendScoreListUpdate("high");
      this.sendScoreListUpdate("average");
      this.statUpdate();
      
      this.pushUpdate({
        type: "current_points",
        data: -1,
      });

      if (this.currentFileScores.length) {
        const mostRecent = this.currentFileScores[this.currentFileScores.length - 1];
        this.pushUpdate({
          type: "new_score",
          data: {
            rank: this.allScores.indexOf(mostRecent) + 1,
            ...mostRecent,
          }
        });
      }
      ws.on("close", () => this.sockets = this.sockets.filter((it) => it != ws));
      ws.on("message", (name: string) => {
        console.log("New score name", name);
        this.userInputName(name);
      })
    });

    const scoreFiles = fs.readdirSync(historyDir).map(
      (fileName) => ({
        fileName,
        creationTime: fs.statSync(pathJoin(historyDir, fileName)).mtime.getTime(),
      })
    ).sort((a, b) => b.creationTime - a.creationTime);
    
    console.log(scoreFiles);

    const now = new Date();
    this.currentFileName = `${Settings.eventName} (${now.toISOString()}).json`;

    const withinRollOff = Date.now() - Settings.rollOff;
    const recentEnoughScoreFiles = scoreFiles.filter((file) => file.creationTime >= withinRollOff);
    console.log(recentEnoughScoreFiles, withinRollOff);
    recentEnoughScoreFiles.map((scoreFile) => {
      const filePath = pathJoin(historyDir, scoreFile.fileName);
      return JSON.parse(fs.readFileSync(filePath) + "");
    }).forEach(
      (scoreList) => this.allScores = this.allScores.concat(scoreList),
    );

    this.allScores.sort((a, b) => b.points - a.points);
    console.log(this.allScores);

    this.gameStats.plays = this.allScores.length;
    this.allScores.forEach((score) => {
      this.gameStats.hits += score.hits || 0;
      this.gameStats.shots += score.shots || 0;
      if (score.loser === false) {
        this.gameStats.wins++;
      }
    })
  }

  public resetPoints() {
    this.currentPoints = 0;
    this.currentShots = 0;
    this.currentHits = 0;
    this.pushUpdate({
      type: "current_points",
      data: -1,
    });
  }

  public currentScore() {
    return this.currentPoints;
  }

  public getCurvedScore() {
    let out = Math.pow(this.currentPoints, 1.23456789) % 999999;
    out += (100 * this.currentHits) * Math.pow(this.currentHits/this.currentShots, 2);
    return ~~out;
  }

  public addPoints(points: number) {
    this.currentPoints += points;
    this.pushUpdate({
      type: "current_points",
      data: this.getCurvedScore(),
    });
  }

  public addShot() {
    this.currentShots++;
    this.gameStats.shots++;
    this.statUpdate();
  }

  public addHit() { 
    this.currentHits++; 
    this.gameStats.hits++;
    this.statUpdate();
  }

  private userInputName(name: string) {
    const score = this.currentFileScores[this.currentFileScores.length - 1];
    score.player = name;
    this.writeOutCurrentScores();
    this.sendScoreListUpdate("high");
    this.sendScoreListUpdate("average");
  }

  private writeOutCurrentScores() {
    fs.writeFile(
      pathJoin(historyDir, this.currentFileName), 
      JSON.stringify(this.currentFileScores, null, 2),
      () => null,
    );
  }

  private statUpdate() {
    this.pushUpdate({
      type: "stats",
      data: this.gameStats,
    })
  }

  public pushScore(loser: boolean) {
    const points = this.getCurvedScore();
    const shots = this.currentShots;
    const hits = this.currentHits;
    this.resetPoints();

    if (!loser) { this.gameStats.wins++; }
    this.gameStats.plays++;
    this.statUpdate();
    
    const addMe: IScore = {
      loser,
      points,
      date: new Date(),
      shots,
      hits,
    };
    this.currentFileScores.push(addMe);
    console.log("New score added: ", addMe);
    
    const allScores = this.allScores;
    let target = allScores.findIndex((score) => score.points < points);
    if (target < 0) {
      target = allScores.length;
      allScores.push(addMe);
    } else {
      allScores.splice(target, 0, addMe);
    }

    let scoreType: ScoreType = "UNREMARKABLE";
    if (target <= 8) {
      scoreType = "HIGH_SCORE";
      this.sendScoreListUpdate("high");
    } else if (Math.abs((allScores.length)/2 - target) < 5) {
      scoreType = "AVG_SCORE";
      this.sendScoreListUpdate("average");
    }

    this.pushUpdate({
      type: "new_score",
      data: {
        rank: target + 1,
        points,
        shots,
        hits,
        type: scoreType,
      } as any,
    });

    this.writeOutCurrentScores();
    return scoreType;
  }


  private sendScoreListUpdate(type: "high" | "average") {
    // let scores: IScore[];
    // let update: IScoreUpdate;
    if (type === "high") {
      this.pushUpdate({
        type: "high_scores",
        data: this.allScores.slice(0, 9),
      });
    } else {
      const middle = ~~(this.allScores.length / 2);
      const middleScore = this.allScores[middle];
      const middlePoints = middleScore ? middleScore.points : 0;
      const startAt = Math.max(0, middle - 5);
      // console.log(this.allScores, middle, startAt);
      const middleChunk = this.allScores.slice(startAt, startAt + 9);
      const sortedByCenter = middleChunk.sort((a, b) => Math.abs(a.points - middlePoints) - Math.abs(b.points - middlePoints))
      const scores = sortedByCenter.slice(0, 9);
      // console.log("AVG", middleChunk, sortedByCenter, scores);
      this.pushUpdate({
        type: "average_scores",
        data: scores,
      })
    }

    // this.pushUpdate(update);
  }

  private pushUpdate(update: IScoreUpdate) {
    this.sockets.forEach((socket) => socket.send(JSON.stringify(update)));
  }
}