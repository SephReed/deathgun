
export class CancelToken { 
	private _isCanceled = false; 
	public cancel() {
		this._isCanceled = true;
	}
	public isCancelled() {
		return this._isCanceled;
  }
}



export function wait(ms: number, cancelToken?: CancelToken) {
  return new Promise(
    (resolve, reject) => setTimeout(() => {
      if (cancelToken && cancelToken.isCancelled()) {
        reject(TOKEN_CANCEL);
      } else {
        resolve();
      }
    }, ms)
  );
}

const TOKEN_CANCEL = "TOKEN_CANCELLED";

// export async function waitThrow(ms: number, cancelToken: CancelToken) {
//   await wait(ms);
//   stopIfCancelled(cancelToken);
// }

/**
 * Throws a cancel event if cancelled.  Should be caught by an "allow cancel" wrapper
 */
export function stopIfCancelled(cancelToken: CancelToken) {
  if (cancelToken.isCancelled()) { throw TOKEN_CANCEL; }
}

export function cancelFn(fn: (...args: any) => Promise<any>) {
  return async (...args: any) => {
    try {
      await fn(...args);
    } catch(e) {
      if (e !== TOKEN_CANCEL) {
        throw (e);
      }
    }
  }
}

export async function allowCancel<TYPE>(fn: (cancelToken: CancelToken) => Promise<TYPE>): Promise<TYPE | undefined> {
  try {
    const cancelToken = new CancelToken();
    const out = await fn(cancelToken);
    return out;
  } catch(e) {
    if (e !== TOKEN_CANCEL) {
      throw (e);
    }
  }
}





export class Waiter {
  private subs: Waiter[] = [];
  private cancelToken = new CancelToken();

  public async wait(ms: number) {
    await this.troll();
    await wait(ms);
    await this.troll();
  }

  public async troll() {
    if (this.cancelToken.isCancelled()) {
      return new Promise(() => null);
    }
    return Promise.resolve();
  }

  public stop() {
    this.cancelToken.cancel();
    this.subs.forEach((sub) => sub.stop());
  }

  public sub() {
    const sub = new Waiter();
    if (this.cancelToken.isCancelled()) {
      sub.stop();
    }
    this.subs.push(sub);
    return sub;
  }
}


export async function cancelable<TYPE>(fn: (waiter: Waiter) => Promise<TYPE>): Promise<TYPE | undefined> {
  try {
    const waiter = new Waiter();
    const out = await fn(waiter);
    return out;
  } catch(e) {
    if (e !== TOKEN_CANCEL) {
      throw (e);
    }
  }
}