import { Deathgun } from "./src/Deathgun";
import * as http from "http";
const handler = require('serve-handler');

 
const server = http.createServer((request, response) => {
  return handler(request, response);
})
 
server.listen(3042, () => {
  console.log('Running at http://localhost:3042');
});


const deathgun = new Deathgun();
deathgun.start();


