#include <iostream>
#include <chrono>
#include <thread>
#include <string>

using namespace std;

void wait(int ms) {
  std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

char input;
void userInput() {
  string mystr;
  while(true) {
    std::getline(std::cin, mystr);
    std::cout << mystr << std::endl;
    wait(1000);
  }
}




int main() {
  std::thread t1(userInput);

  int count = 0;
  while(true) {
    wait(1000);
    std::cout << count++ << "::" << input << std::endl;
  }

  return 0;
}
