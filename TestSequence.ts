import { Deathgun } from "./src/Deathgun";
import { allowCancel, wait } from "./src/Tools";
import { EyeList, Eye } from "./src/Kraken";
import { Wave, Tentacle } from "./src/IOInterface";


const deathgun = new Deathgun("NO_GAME");


const testType = process.argv[2];

const kraken = deathgun.kraken;

const eyesInOrder = [
  Eye.MAMA_LEFT, Eye.BIG_MAMA, Eye.MAMA_RIGHT,
  Eye.MID_LEFT, Eye.MID_RIGHT,
  Eye.BOTTOM_LEFT, Eye.BOTTOM_CENTER, Eye.BOTTOM_RIGHT,
]

// const pistonsInOrder = [
//   Wave.UPPER_LEFT, Wave.UPPER_RIGHT,
//   Tentacle.MID_LEFT, Tentacle.MID_RIGHT,
//   Tentacle.BOTTOM_LEFT, Tentacle.BOTTOM_CENTER, Tentacle.BOTTOM_RIGHT,
// ]


if (testType === "hit_test") {
  allowCancel(async (cancelToken) => {
    await wait(5000);

    kraken.showAllEyes();
    kraken.allEyesGreen();
    
    await wait(2000);
  
    kraken.addEyeHitListener((eye) => {
      kraken.setEyeState(eye, "red");
      kraken.showEye(eye, false);
    })
  });


} else {
  allowCancel(async (cancelToken) => {
    await wait(5000);
    kraken.hideAndDisableAll();

    await wait(2000);
  
    for (let i = 0; i < eyesInOrder.length; i++) {
      kraken.showEye(eyesInOrder[i]);
      await wait(1000);
    }
  
    for (let i = 0; i < eyesInOrder.length; i++) {
      kraken.setEyeState(eyesInOrder[i], "green");
      await wait(500);
    }
  
    for (let i = 0; i < eyesInOrder.length; i++) {
      kraken.setEyeState(eyesInOrder[i], "red");
      await wait(500);
    }
  
    for (let i = 0; i < eyesInOrder.length; i++) {
      kraken.showEye(eyesInOrder[i], false);
      await wait(1000);
    }
  });
}

