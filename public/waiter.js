let __waiter_id = 0;

class Waiter {
  constructor() {
    this.id = __waiter_id++;

    this.wait = async (ms) => {
      await this.troll();
      await new Promise((resolve) => 
        setTimeout(resolve, ms)
      )
      await this.troll();
    }

    this.setInterval = (fn, ms) => {
      return new Promise((resolve) => {
        const stop = async () => {
          clearInterval(interval);
          await this.troll();
          resolve();
        }
        const interval = setInterval(() => {
          if (this.cancelled) { return clearInterval(interval); }
          fn(stop);
        }, ms);
      })
    }

    this.troll = async () => {
      if (this.cancelled) {
        return new Promise();
      } else {
        return Promise.resolve();
      }
    }
  }

  cancel() { this.cancelled = true; }
  stop() { this.cancel(); }
}
